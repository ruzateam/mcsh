/*
Navicat MySQL Data Transfer

Source Server         : test
Source Server Version : 50541
Source Host           : localhost:3306
Source Database       : Emercom_CRM

Target Server Type    : MYSQL
Target Server Version : 50541
File Encoding         : 65001

Date: 2015-09-14 09:58:38
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `Division`
-- ----------------------------
DROP TABLE IF EXISTS `Division`;
CREATE TABLE `Division` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(1024) NOT NULL,
  `short_name` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Division
-- ----------------------------
INSERT INTO `Division` VALUES ('1', 'Отдел информационных технологий', 'test name');
INSERT INTO `Division` VALUES ('2', 'Отдел охран', 'Отдел Охраны');
INSERT INTO `Division` VALUES ('3', 'Отдел Управления и Информационных технологий', 'Отдел У и ИТ');

-- ----------------------------
-- Table structure for `DivisionBranch`
-- ----------------------------
DROP TABLE IF EXISTS `DivisionBranch`;
CREATE TABLE `DivisionBranch` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `division_id` int(6) NOT NULL,
  `name` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of DivisionBranch
-- ----------------------------
INSERT INTO `DivisionBranch` VALUES ('1', '1', 'ППУ');
INSERT INTO `DivisionBranch` VALUES ('2', '2', 'Охрана');
INSERT INTO `DivisionBranch` VALUES ('3', '2', 'Транспортный отдел');
INSERT INTO `DivisionBranch` VALUES ('4', '3', 'Формирование баз данных и моделирования ЧС');
INSERT INTO `DivisionBranch` VALUES ('5', '3', 'Проблем устойчивости функционирования системы телекоммуникации');
INSERT INTO `DivisionBranch` VALUES ('6', '3', 'Формирование программного обеспечения');

-- ----------------------------
-- Table structure for `Exercise`
-- ----------------------------
DROP TABLE IF EXISTS `Exercise`;
CREATE TABLE `Exercise` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `theme_id` int(6) DEFAULT NULL,
  `number` int(6) DEFAULT NULL,
  `name` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Exercise
-- ----------------------------

-- ----------------------------
-- Table structure for `Groups`
-- ----------------------------
DROP TABLE IF EXISTS `Groups`;
CREATE TABLE `Groups` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `str_role` varchar(256) DEFAULT NULL,
  `int_role` int(1) DEFAULT NULL,
  `perms_id` varchar(4096) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Groups
-- ----------------------------
INSERT INTO `Groups` VALUES ('1', 'Администратор', '0', '[5]');
INSERT INTO `Groups` VALUES ('2', 'Пользователь', '1', '[1]');

-- ----------------------------
-- Table structure for `Images`
-- ----------------------------
DROP TABLE IF EXISTS `Images`;
CREATE TABLE `Images` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `news_id` int(6) NOT NULL DEFAULT '0',
  `path` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Images
-- ----------------------------
INSERT INTO `Images` VALUES ('1', '113', '8a0271fd936ba203ea68252c8ab276f9');
INSERT INTO `Images` VALUES ('2', '112', '95ea1426b5a44774ef9416865d1c97b8');
INSERT INTO `Images` VALUES ('3', '119', '71bb546bdfb463c25cc97cf4a4e45504');
INSERT INTO `Images` VALUES ('4', '120', 'b9fffc46d97cbbf14355866a1319ef01');
INSERT INTO `Images` VALUES ('5', '142', '1c74905a50b636380d514285787f2752');
INSERT INTO `Images` VALUES ('8', '143', '98519d4589107fa032c0988eb84baf13');
INSERT INTO `Images` VALUES ('9', '139', '64805bef52c9e8bcd29fe6cccae96e32');
INSERT INTO `Images` VALUES ('10', '144', 'fe8b58450f8ab4b20ce1a17d56bd6dce');
INSERT INTO `Images` VALUES ('11', '144', '2a3888fd00fe09880f2727d776b9a472');
INSERT INTO `Images` VALUES ('12', '144', 'fbf2850f0df995884f1e67507bab613d');
INSERT INTO `Images` VALUES ('13', '142', 'a64edd21046f35fb9de3ab591317fdcb');
INSERT INTO `Images` VALUES ('14', '143', '0bf957a6b1b7688ab670cc03a2576975');

-- ----------------------------
-- Table structure for `Journals`
-- ----------------------------
DROP TABLE IF EXISTS `Journals`;
CREATE TABLE `Journals` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `date_begin` date DEFAULT NULL,
  `division_id` int(6) DEFAULT NULL,
  `division_branch_id` int(6) DEFAULT NULL,
  `master_division_id` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Journals
-- ----------------------------
INSERT INTO `Journals` VALUES ('1', '2015-06-25', '3', '4', '1');
INSERT INTO `Journals` VALUES ('2', '2015-01-25', '1', '1', '2');
INSERT INTO `Journals` VALUES ('3', '2015-09-22', '2', '2', '1');
INSERT INTO `Journals` VALUES ('4', '2015-09-22', '2', '5', '3');
INSERT INTO `Journals` VALUES ('5', '2015-09-14', '2', '4', '1');
INSERT INTO `Journals` VALUES ('6', '2015-09-14', '2', '2', '4');

-- ----------------------------
-- Table structure for `Leasons`
-- ----------------------------
DROP TABLE IF EXISTS `Leasons`;
CREATE TABLE `Leasons` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `jornal_id` int(6) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `subject_id` int(6) DEFAULT NULL,
  `theme_id` int(6) DEFAULT NULL,
  `exercise_id` int(6) DEFAULT NULL,
  `time` int(2) DEFAULT NULL,
  `master_id` int(6) DEFAULT NULL,
  `scores` mediumtext,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Leasons
-- ----------------------------

-- ----------------------------
-- Table structure for `Masters`
-- ----------------------------
DROP TABLE IF EXISTS `Masters`;
CREATE TABLE `Masters` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `division_id` int(6) DEFAULT NULL,
  `post` varchar(1024) DEFAULT NULL,
  `range_id` int(6) DEFAULT NULL,
  `name` varchar(1024) DEFAULT NULL,
  `second_name` varchar(1024) DEFAULT NULL,
  `last_name` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Masters
-- ----------------------------
INSERT INTO `Masters` VALUES ('1', '3', 'Начальник отделения', '7', 'А', 'Фотин', 'М');
INSERT INTO `Masters` VALUES ('2', '3', 'Начальник отделения', '8', 'К', 'Агапов', 'А');
INSERT INTO `Masters` VALUES ('3', '3', 'Начальник отдела', '10', 'Д', 'Ивановский', 'М');
INSERT INTO `Masters` VALUES ('4', '3', 'Старший инженер', '8', 'Д', 'Шапурин', 'С');

-- ----------------------------
-- Table structure for `News`
-- ----------------------------
DROP TABLE IF EXISTS `News`;
CREATE TABLE `News` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `title` varchar(1024) DEFAULT NULL,
  `body` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=145 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of News
-- ----------------------------
INSERT INTO `News` VALUES ('112', '2015-08-31 09:25:36', '3123123123', '<p style=\"font-style: italic;\"><strong>Тест нового редактора</strong></p>\r\n');
INSERT INTO `News` VALUES ('114', '2015-09-01 10:29:21', 'test title', '<p>новая новость крч &nbsp;<strong>fgsdfgsdfg</strong></p>\r\n');
INSERT INTO `News` VALUES ('118', '2015-09-01 11:20:05', 'etst', '<p>test</p>\r\n');
INSERT INTO `News` VALUES ('119', '2015-09-02 09:32:11', 'Новоя новость ', '<p>Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;Текст, много текста.&nbsp;</p>\r\n');
INSERT INTO `News` VALUES ('120', '2015-09-02 10:23:39', 'qwerty', '<p>qwer</p>\r\n');
INSERT INTO `News` VALUES ('124', '2015-09-02 18:04:40', '', '');
INSERT INTO `News` VALUES ('125', '2015-09-02 18:04:58', '', '');
INSERT INTO `News` VALUES ('126', '2015-09-02 18:06:50', '', '');
INSERT INTO `News` VALUES ('127', '2015-09-02 18:07:04', '', '');
INSERT INTO `News` VALUES ('128', '2015-09-02 18:08:16', '', '');
INSERT INTO `News` VALUES ('129', '2015-09-02 18:08:25', '', '');
INSERT INTO `News` VALUES ('130', '2015-09-02 18:08:40', '', '');
INSERT INTO `News` VALUES ('131', '2015-09-02 18:08:47', '', '');
INSERT INTO `News` VALUES ('132', '2015-09-02 18:09:03', '', '');
INSERT INTO `News` VALUES ('133', '2015-09-02 18:11:23', '', '');
INSERT INTO `News` VALUES ('134', '2015-09-02 18:13:48', '', '');
INSERT INTO `News` VALUES ('135', '2015-09-02 18:17:36', '', '');
INSERT INTO `News` VALUES ('136', '2015-09-02 18:18:11', '', '<p>asdf</p>\r\n');
INSERT INTO `News` VALUES ('137', '2015-09-02 18:23:53', '', '');
INSERT INTO `News` VALUES ('138', '2015-09-02 18:24:21', '', '');
INSERT INTO `News` VALUES ('139', '2015-09-02 18:24:31', 'qwerty', '<p>qwer</p>\r\n');
INSERT INTO `News` VALUES ('140', '2015-09-02 18:24:45', '', '');
INSERT INTO `News` VALUES ('142', '2015-09-03 17:39:11', 'Курсанты-первокурсники кова', '<p>3 сентября в ходе своей работы в Приморском крае Глава МЧС России Владимир Пучков посетил Дальневосточную пожарно-спасательную академию МЧС России, находящуюся на о. Русский.</p>\r\n\r\n<p>Владимир Пучков провел торжественное приведение к присяге на верность Российской Федерации и своему профессиональному долгу курсантов первого курса, это 43 юноши и 8 девушек. Также в мероприятии приняли участие представители администрации Приморского края и города Владивостока, а также ветераны пожарной охраны Приморского края.</p>\r\n\r\n<p>Кроме того, глава МЧС России вручил ведомственные награды отличившимся сотрудникам, а также медаль &laquo;За спасение погибающих на водах&raquo; получила Марии Рябушкиной ученице 8 класса школы № 5 села Лазо Дальнереченского района за спасение из воды двухлетнего ребенка, нагрудный знак &laquo;Участнику ликвидации последствий ЧС&raquo; получил Данил Бабенко - ученик 4-го класса из села Духовское Спасского района, также успевший в свои юные годы спасти жизнь человеку.</p>\r\n\r\n<p>Гостям мероприятия была представлена выставка пожарно-спасательной техники, стоящей на вооружении подразделений МЧС России, фотовыставка о деятельности МЧС России &laquo;Мы первыми приходим на помощь и служим людям&raquo;. Были продемонстрированы выступления сборной команды Приморского края по пожарно-прикладному спорту, боевое развёртывание расчета учебной пожарно-спасательной части академии, а также демонстративные полеты авиации МЧС России.</p>\r\n\r\n<p>После чего Владимир Пучков выступил перед курсантами с лекцией на тему &laquo;МЧС 2030&raquo;.</p>\r\n\r\n<p><strong>Для справки:</strong>&nbsp;своё существование ВУЗ начал 1 сентября 2013 года, на его открытии присутствовал глава МЧС России Владимир Пучков. В настоящее время обучение в Академии осуществляется по специальности &laquo;Пожарная безопасность&raquo; с присвоением квалификации &ndash; &laquo;Инженер пожарной безопасности&raquo;.</p>\r\n\r\n<p>В процессе подготовки специалистов Дальневосточная пожарно-спасательная Академия МЧС России будет тесно сотрудничать с Дальневосточным федеральным университетом.</p>\r\n\r\n<p>Сегодня в системе МЧС России 7 высших учебных заведений федерального подчинения, они готовят специалистов для МЧС России. Это современные научно-образовательные комплексы, глубоко интегрированные в мировое научно-образовательное пространство. В процессе обучения применяются современные информационные системы и образовательные технологии.</p>\r\n');
INSERT INTO `News` VALUES ('143', '2015-09-03 17:40:42', 'Операция по спасению зверей из подтопленного зоопарка Уссурийска завершена', '<p>Сегодня все животные из подтопленного зоопарка в городе Уссурийске эвакуированы. Последними свой вольер покинули три гималайских медведя, которых спасатели вместе с ветеринарами, экологами и сотрудниками зоопарка перевезли на лодках на сушу. Затем, погрузив в клетки, транспортировали в место их временного пребывания.</p>\r\n\r\n<p>Операция по спасению животных длилась три дня. Специально для проведения этих работ в Уссурийск прибыл вертолёт Ми-26 Хабаровского авиационно-спасательного центра МЧС России.</p>\r\n');
INSERT INTO `News` VALUES ('144', '2015-09-06 12:43:34', 'asdf', '<p>asdf</p>\r\n');

-- ----------------------------
-- Table structure for `Permissions`
-- ----------------------------
DROP TABLE IF EXISTS `Permissions`;
CREATE TABLE `Permissions` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `key` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Permissions
-- ----------------------------
INSERT INTO `Permissions` VALUES ('1', 'Тест пермов 1', 'test');
INSERT INTO `Permissions` VALUES ('2', 'Тест пермов 2', 'test2');
INSERT INTO `Permissions` VALUES ('3', 'Тест пермов 3', 'test3');
INSERT INTO `Permissions` VALUES ('4', 'Тест пермов 4', 'test4');
INSERT INTO `Permissions` VALUES ('5', 'Редактирование и удаление новостей', 'edit_delete_news');

-- ----------------------------
-- Table structure for `Personal`
-- ----------------------------
DROP TABLE IF EXISTS `Personal`;
CREATE TABLE `Personal` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `division_branch_id` int(6) NOT NULL,
  `name` varchar(512) DEFAULT NULL,
  `second_name` varchar(512) DEFAULT NULL,
  `last_name` varchar(512) DEFAULT NULL,
  `range_id` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Personal
-- ----------------------------
INSERT INTO `Personal` VALUES ('1', '2', 'Денис2', 'Grachev', 'Gennad\'evich', '4');

-- ----------------------------
-- Table structure for `Range`
-- ----------------------------
DROP TABLE IF EXISTS `Range`;
CREATE TABLE `Range` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `full_name` varchar(512) DEFAULT NULL,
  `short_name` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Range
-- ----------------------------
INSERT INTO `Range` VALUES ('1', 'рядовой', 'ряд');
INSERT INTO `Range` VALUES ('2', 'ефрейтор', 'ефр');
INSERT INTO `Range` VALUES ('3', 'младший сержант', 'мл с-т');
INSERT INTO `Range` VALUES ('4', 'сержант', 'с-т');
INSERT INTO `Range` VALUES ('5', 'старший сержант', 'ст с-т');
INSERT INTO `Range` VALUES ('6', 'младший лейтенант', 'мл л-т');
INSERT INTO `Range` VALUES ('7', 'лейтенант', 'л-т');
INSERT INTO `Range` VALUES ('8', 'старший лейтенант', 'ст л-т');
INSERT INTO `Range` VALUES ('9', 'майор', 'м-р');
INSERT INTO `Range` VALUES ('10', 'подполковник', 'п/пк');
INSERT INTO `Range` VALUES ('11', 'полковник', 'пк');

-- ----------------------------
-- Table structure for `Scores`
-- ----------------------------
DROP TABLE IF EXISTS `Scores`;
CREATE TABLE `Scores` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `lesson_id` int(6) DEFAULT NULL,
  `human_id` int(6) DEFAULT NULL,
  `score` int(1) DEFAULT NULL,
  `work` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Scores
-- ----------------------------

-- ----------------------------
-- Table structure for `Sessions`
-- ----------------------------
DROP TABLE IF EXISTS `Sessions`;
CREATE TABLE `Sessions` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` varchar(256) DEFAULT NULL,
  `user_ip` varchar(16) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Sessions
-- ----------------------------
INSERT INTO `Sessions` VALUES ('1', '4u5qp61275qv5p104c4ssr57i1', '10.25.6.176', '2015-08-28 09:24:55');
INSERT INTO `Sessions` VALUES ('2', 'um5bbklh3p4h0buhbhn670dnj0', '10.25.6.176', '2015-08-29 11:46:24');
INSERT INTO `Sessions` VALUES ('3', 'um5bbklh3p4h0buhbhn670dnj0', '10.25.6.176', '2015-08-29 11:47:23');
INSERT INTO `Sessions` VALUES ('4', 'um5bbklh3p4h0buhbhn670dnj0', '10.25.6.176', '2015-08-29 11:47:38');
INSERT INTO `Sessions` VALUES ('5', 'um5bbklh3p4h0buhbhn670dnj0', '10.25.6.176', '2015-08-29 11:48:00');
INSERT INTO `Sessions` VALUES ('6', 'um5bbklh3p4h0buhbhn670dnj0', '10.25.6.176', '2015-08-29 11:49:41');
INSERT INTO `Sessions` VALUES ('7', 'um5bbklh3p4h0buhbhn670dnj0', '10.25.6.176', '2015-08-29 11:53:29');
INSERT INTO `Sessions` VALUES ('8', 'um5bbklh3p4h0buhbhn670dnj0', '10.25.6.176', '2015-08-29 11:54:04');
INSERT INTO `Sessions` VALUES ('9', '9glv7bfb7apsof7s07ohpaq594', '127.0.0.1', '2015-08-30 15:28:43');
INSERT INTO `Sessions` VALUES ('10', 'go50qac5fu6iof1an5phv49f85', '127.0.0.1', '2015-08-31 09:25:19');
INSERT INTO `Sessions` VALUES ('11', 'go50qac5fu6iof1an5phv49f85', '127.0.0.1', '2015-08-31 11:58:52');
INSERT INTO `Sessions` VALUES ('12', 'tc0v43ejk0hhb58e9n8i44t8r5', '127.0.0.1', '2015-09-01 09:47:43');
INSERT INTO `Sessions` VALUES ('13', '9kk5ar7mdvj19177eol2lt2je7', '::1', '2015-09-03 17:46:36');
INSERT INTO `Sessions` VALUES ('14', '9kk5ar7mdvj19177eol2lt2je7', '::1', '2015-09-03 17:47:01');
INSERT INTO `Sessions` VALUES ('15', '115nf25cuo7tho0gk8gva7crt1', '::1', '2015-09-06 11:30:40');
INSERT INTO `Sessions` VALUES ('16', '0tb0u4vme7jbvukeo2b8l3prp7', '::1', '2015-09-06 15:07:35');
INSERT INTO `Sessions` VALUES ('17', 'ageq6rfbh8650dlebttq7sh5i2', '::1', '2015-09-06 15:55:14');
INSERT INTO `Sessions` VALUES ('18', 'l8u9k3ilvi59okt86af080d6m4', '::1', '2015-09-07 10:42:19');
INSERT INTO `Sessions` VALUES ('19', 'chielcoalahsmvv8e94ikb4ad6', '::1', '2015-09-07 12:27:47');
INSERT INTO `Sessions` VALUES ('20', 'ea9efhdfpkhok3thl6lkjctjk5', '::1', '2015-09-07 15:21:54');
INSERT INTO `Sessions` VALUES ('21', 'h8l2o198vg1epej57rv1ckcoq7', '::1', '2015-09-08 09:43:30');
INSERT INTO `Sessions` VALUES ('22', '4u6lguqjnrc7st1a7l6g9bj252', '::1', '2015-09-12 17:10:55');

-- ----------------------------
-- Table structure for `Settings`
-- ----------------------------
DROP TABLE IF EXISTS `Settings`;
CREATE TABLE `Settings` (
  `id` int(3) NOT NULL DEFAULT '0',
  `name` varchar(256) DEFAULT NULL,
  `setting` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Settings
-- ----------------------------
INSERT INTO `Settings` VALUES ('1', 'version', '0.1 betta');

-- ----------------------------
-- Table structure for `Subject`
-- ----------------------------
DROP TABLE IF EXISTS `Subject`;
CREATE TABLE `Subject` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Subject
-- ----------------------------

-- ----------------------------
-- Table structure for `Themes`
-- ----------------------------
DROP TABLE IF EXISTS `Themes`;
CREATE TABLE `Themes` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(4096) DEFAULT NULL,
  `number` int(6) DEFAULT NULL,
  `subject_id` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Themes
-- ----------------------------

-- ----------------------------
-- Table structure for `Users`
-- ----------------------------
DROP TABLE IF EXISTS `Users`;
CREATE TABLE `Users` (
  `id` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(256) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `avatar` varchar(512) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  `second_name` varchar(64) DEFAULT NULL,
  `last_name` varchar(64) DEFAULT NULL,
  `mission_login` int(1) DEFAULT NULL,
  `last_ip` varchar(16) DEFAULT NULL,
  `role_id` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of Users
-- ----------------------------
INSERT INTO `Users` VALUES ('6', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'df2c762bbbc8e2c5b74d8e747fd09849', 'admin', 'admin', 'admin', null, '::1', '0');
