<div class="row ">
	<?if ($status === true) {?>
		<div class="alert alert-notice" style="background: lightgreen;text-align:center;border-color:black;" > 
			<div style="color: green;" >
				Пользователь создан успешно
			</div>
		</div>
	<?} else if($status === false) {?>
		<div class="alert alert-notice" style="background: lightcoral;text-align:center;border-color:black;" > 
			<div style="color: red;" >
				При сохранении возникли проблемы
			</div>
		</div>
	<?};?>
    <div style="text-align: center">
        <div style="font: 400% serif;">
        	<i>
        	Пользователи
        	</i>
        </div>
    </div>
    <div class='form-actions'>
    	<a href="/users/new">
	    	<div class="btn btn-primary" style="height:40px; align:center; width:100%;margin-top: 30px;"	>
		    	<span class="glyphicon glyphicon-plus"  style="font: 150% serif;">Добавить пользователя</span>
			</div>
		</a>
	</div>
    
	<div class="row show-grid" style="background-color: rgba(86,61,124,.15);border: 1px solid rgba(86,61,124,.2);   margin-top:30px; padding-top: 10px;">
		<div class="col-md-1">Id</div>
		<div class="col-md-1">Логин</div>
		<div class="col-md-1">Имя</div>
		<div class="col-md-1">Фамилия</div>
		<div class="col-md-1">Отчество</div>
		<div class="col-md-1">Заход с IP</div>
		<div class="col-md-1">Роль</div>
	</div>

	<?foreach ($UsersArray as $key=>$user){?>
		<div class="row"  style="background-color: rgba(86,61,124,.15);border: 1px solid rgba(86,61,124,.2);    margin-top:10px;padding-top: 10px;">
			<div class="col-md-1"><?=$user['id'];?></div>
			<div class="col-md-1"><?=$user['login'];?></div>
			<div class="col-md-1"><?=$user['name'];?></div>
			<div class="col-md-1"><?=$user['secondName'];?></div>
			<div class="col-md-1"><?=$user['lastName'];?></div>
			<div class="col-md-1"><?=$user['lastIp'];?></div>
			<div class="col-md-1"><?=$user['role'];?></div>
			<div class="pull-rigth" style="float:right;">
				<div class="col-md-1" style="display:inline-block;white-space:nowrap;">
				<a href=<?="/users/edit/".$user['id'];?> name="edit" >
		    			<span class="glyphicon glyphicon-edit"></span>	
		    	</a>
		    	<a name="sys-delete-user">
		    			<span class="glyphicon glyphicon-trash"></span>
		    			<input type="hidden" value=<?=$user['id'];?> >
		    	</a>	
				</div>
			</div>
		</div>
	<?};?>
   

</div>
