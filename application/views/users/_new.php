<div class="row">
	<div style="text-align: center">

        <div style="font: 400% serif;">
        	<i>
        	<?=	$this -> Configs -> Action == 'edit' ? 'Редактирование пользователя' :'Создание пользователя';?>
        	</i>
        </div>
    </div>
	 <form accept-charset="UTF-8" method="post" enctype="multipart/form-data" action=<?=($this->Config->Action == "edit" ? '/users/update' : '/users/create')?>><div style="display:none"><input name="utf8" type="hidden" /><input name="authenticity_token" type="hidden" value="" /></div>
			<div class='form-group'>
				<label for="login">Логин</label>
				<input class="form-control" placeholder="Введите логин" id="_login" name="login" type="text" />
	 		</div>
	 		<div class='form-group'>
				<label for="name">Имя</label>
				<input class="form-control" placeholder="Введите имя" id="_name" name="name"></input>
	 		</div>
	 		<div class='form-group'>
				<label for="second_name">Фамилия</label>
				<input class="form-control" placeholder="Введите фамилию" id="_second_name" name="second_name"></input>
	 		</div>
	 		<div class='form-group'>
				<label for="last_name">Отчество</label>
				<input class="form-control" placeholder="Введите отчество" id="_last_name" name="last_name"></input>
	 		</div>
	 		<div class='form-group'>
				<label for="password">Пароль</label>
				<input class="form-control" placeholder="Введите пароль" id="_password" name="password"></input>
	 		</div>
	 	
	 		<div class='form-group'>
				<label for="role">Права доступа</label>

				<select class="form-control selectpicrke" name="role" id="_role">
					<?foreach ($groups as $group) {;?>
						<option value=<?=$group['intRole']?>><?=$group['strRole'];?></option>
					<?};?>
				</select>
	 		</div>


	 		<div class='form-group'>
				<label for="avatar">Загрузка аватара</label>
				<input class="form-control" id="_avatar" name="avatar" type="file"></input	>
	 		</div>

	 		<div class='form-actions'>
				<input class="btn btn-primary btn-md" name="commit" type="submit" value="Сохранить" />
			</div>
	 </form>
</div>