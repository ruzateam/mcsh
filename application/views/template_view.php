<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta content="IE=Edge,chrome=1" http-equiv="X-UA-Compatible">
		<meta content="width=device-width, initial-scale=1.0" name="viewport">
		<title>Корпаративная система МЧС</title>
		<link href="<?=BASE_URL;?>application\static\bootstrap\css\bootstrap.min.css" media="all" rel="stylesheet" />
		<link href="<?=BASE_URL;?>application\static\bootstrap\css\bootstrap-theme.min.css" media="all" rel="stylesheet" />
		<link href="<?=BASE_URL;?>application\static\template\css\template.css" media="all" rel="stylesheet" />
		<link href="<?=BASE_URL;?>application\static\datatables\css\jquery.dataTables.css" media="all" rel="stylesheet" />
		<link href="<?=BASE_URL;?>application\static\datatables\css\jquery-ui.css" media="all" rel="stylesheet" />
		<script type="text/javascript" src="<?=BASE_URL;?>application\static\template\js\jquery.js"></script>
		<script type="text/javascript" src="<?=BASE_URL;?>application\static\bootstrap\js\bootstrap.min.js"></script>
		<script type="text/javascript" src="<?=BASE_URL;?>application\static\template\js\API.js"></script>
		<?if($this -> Configs -> Action == 'index' && $this -> Configs -> Controller_name == 'news'){?>
			<script type="text/javascript" src="<?=BASE_URL;?>application\static\tosrus\src/js/jquery.tosrus.min.all.js"></script>
			<link href="<?=BASE_URL;?>application\static\tosrus\src\css\jquery.tosrus.all.css" media="all" rel="stylesheet" />
		<?};?>
		<?if($this -> Configs -> Controller_name == 'workbench') {?>
			<script type="text/javascript" src="<?=BASE_URL;?>application\static\template\js\workbench.js"></script>
		<?};?>
		<script type="text/javascript" src="<?=BASE_URL;?>application\static\template\js\application.js"></script>

		<script type="text/javascript" src="<?=BASE_URL;?>application\static\template\js\raphael.min.js"></script>
		<?if($this -> Configs -> Action == 'create' or $this -> Configs -> Action == 'new') {?>
			<script type="text/javascript" src="<?=BASE_URL;?>application\static\template\js\ckeditor\ckeditor.js"></script>
		<?};?>
	</head>
	<body>
		<input type="text" name="sys-action" class="sys-action" hidden="true" value="<?=$this -> Configs -> Action ?>">
		<input type="text" name="sys-controller" class="sys-controller" hidden="true" value="<?=$this -> Configs -> Controller_name ?>">
		<div id="wrapper">
			<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
				<button class="navbar-toggle" data-target=".navbar-ex1-collapse" data-toggle="collapse" type="button">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#" ><div class="mchs-header">МЧС</div> России</a>
			<div class="mobile-menu">
				<a class="navbar-brand pull-right" href="/login/logout">Выход</a>
				<a class="navbar-brand pull-right" href="#"><?=$this -> User -> login;?>(<?=$this -> User -> str_role;?>)</a>
			</div>
			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav side-nav">
					<li><a class=<?=($this -> Configs -> Controller_name == 'news'  ? 'active' :"");?> "" href="/news"> Новости</a></li>
					<li><a class= <?=($this -> Configs -> Controller_name == 'journal'  ? 'active' :"");?> "" href="/journal"> Журнал</a></li>
					<li><a class=<?=($this -> Configs -> Controller_name == 'users'  ? 'active' :"");?> "" href="/users"> Пользователи </a></li>
					<li><a class=<?=($this -> Configs -> Controller_name == 'group_perms'  ? 'active' :"");?> "" href="/group_perms"> Группы и Разрешения </a></li>
					<!-- <li><a class=<?=($this -> Configs -> Controller_name == 'workbench'  ? 'active' :"");?> "" href="/workbench"> Рабочая Зона </a></li> -->
					<li>	
						<a class=<?=($this -> Configs -> Controller_name == 'workbench'  ? 'active' :"");?> "" id='sys-workbranch-dropdown'  href="#"> Рабочая Зона </a>
						<div class="dropdown">
					 
						  <ul class="workbranch-dropdown-menu" style='display:none' aria-labelledby="dropdownMenu1">
						    <li><a href="/workbench/division">Отдел</a></li>
						    <li><a href="/workbench/division_bench">Отделения</a></li>
						    <li><a href="/workbench/personal">Личный состав</a></li>
						    <li><a href="/workbench/journal">Журнал</a></li>
						  </ul>
						</div>
					</li>
				</ul>
			
			</div>
			</nav>
		<div id="page-wraper">
			<div class="container">
				<div class="admin-content">
					<?=$body?>
			</div>
		</div>
	</body>
</html>