<!DOCTYPE html>
<html lang='en'>
	<head>
		<meta charset='utf-8'>
		<meta content='width=device-width, initial-scale=1.0' name='viewport'>
		<link href="application\static\bootstrap\css\bootstrap.min.css" media="all" rel="stylesheet" />
		<link href="application\static\bootstrap\css\bootstrap-theme.min.css" media="all" rel="stylesheet" />
		<link href="application\static\login\css\login.css" media="all" rel="stylesheet" />
	</head>
	<body>
		<div class='container'>
		<div class='content'>
			<div class='row'>
				<div class='span9'>
					<div class='narrow'>
						<div class='form-login'>
							<div class='page-header'>
								
							<b><h1>Авторизация</h1></b>
							</div>
									<form accept-charset="UTF-8" method="post" action="<?=BASE_URL?>login"><div style="display:none"><input name="utf8" type="hidden" value="&#x2713;" /><input name="authenticity_token" type="hidden" value="" /></div>
										<div class='form-group'>
											<label for="login">Логин</label>
											<input class="form-control" id="login_" name="login" type="text" />
										</div>
										<div class='form-group'>
											<label for="password">Пароль</label>
											<input class="form-control" id="password" name="password" type="password" />
										</div>
										<div class='form-actions'>
											<input class="btn btn-primary btn-md" name="commit" type="submit" value="Войти" /><div class="login-err-msg"><?=$message?></div>
										</div>
									</form>
                                    
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
