<div class="row">
    <div style="text-align: center">

        <div style="font: 400% serif;">
        	<i>
        	<!-- заголовок списка -->
        	</i>
        </div>
    </div>
    <div class='form-actions'>
    	<a href="/news/new">
	    	<div class="btn btn-primary btn-create-news" style="height:40px; align:center;"	>
		    	<span class="glyphicon glyphicon-plus"  style="font: 150% serif;">Добавить новость</span>
			</div>
		</a>
	</div>
    <div class="list-group">
    	<div class = "sys-news-item sys-news-item-hidden" style="display:none">
		    <div class="list-group-item list-news-item" >

		    	<div class="list-group-item-heading sys-heading-news">
		        <!-- title News -->
		       </div>
		       <div class="pull-left pull-left-news"  style="display:none">
			           <div class="col-md-3 sys-news-img">
			           <!-- origin img  -->
			           		<div class="sys-origin-img">
			           			<a href="" style="display:none;">
			           				<img src=''  class="news-img"/>
			           			</a>
			           		</div>
			           		<div class="preview-img sys-preview-img">
				           		<a href="" style="display:none;">
				           			<img src="" width="30px" height="30px" />
				           		</a>
			           		</div>
			           </div>
		       </div>	
		      	<div class="sys-context-news">
		      	<!-- context -->
		   		</div>
		        
		    </div>
		    <div style="display:inline-block;width:100%;">
		    	<?if($this->Library->ACS->checkPerm('edit_delete_news')){?>
			    	<div class="pull-right" style="font-size:200%;">
			    		<a href="#" name="edit" >
			    			<span class="glyphicon glyphicon-edit"></span>	
			    		</a>
			    		<a name="delete" >
			    			<span class="glyphicon glyphicon-trash"></span>
			    			<input type="hidden">
			    		</a>
			    	</div>
		    	<?};?>
			    <div class="pull-left sys-all" style="color:blue;">
			        	<ul class="pager">
							<li><a>Подробнее</a></li>
						</ul>
			    </div>

		    </div>
		    </div>
	    	<div class="clearfix"></div>
	    </div>
	    <input name="sys-before" type="hidden" />
	    </div>
		<div class="sys-current-page">
			<input type="hidden" name="page" value='1' /> 
			<input type="hidden" name="count" value='10	' /> 
		</div>
   </div>

   

</div>
