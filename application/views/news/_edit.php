<div class="row">
	<div style="text-align: center">

        <div style="font: 400% serif;">
        	<i>
        	<?=	$this -> Configs -> Action == 'edit' ? 'Редактирование новости' :'Создание новости';?>
        	</i>
        </div>
    </div>
	 <form accept-charset="UTF-8" method="post" enctype="multipart/form-data"><div style="display:none"><input name="utf8" type="hidden" /><input name="authenticity_token" type="hidden" value="" /></div>
	 <input name="news_id" type="text" hidden="true" value="<?=	$this -> Configs -> Action == 'edit' ? $this->Configs->Args[0] : ''?>">
			<div class='form-group'>
				<label for="title">Заголовок</label>
				<input class="form-control" id="title_" name="title" type="text" placeholder="Введите текст" value="<?=$bodyNews['title']?>"/>
	 		</div>
	 		<div class='form-group'>
				<label for="context">Содержание</label>
				<textarea class="form-control" style="height:300px;" placeholder="Введите текст" id="body" name="body"><?=$bodyNews['body']?></textarea>
	 		</div>
			<script>
			   CKEDITOR.replace('body');
			</script>
	 		<div class='form-group img-news'>
				<label for="img">Картинки</label><br><div style="display: inherit">
				<?
				if(!empty($bodyNews['imgs'])){
					foreach($bodyNews['imgs'] as $Image){
						echo '<div id="'.$Image['image_id'].'"><img src="'.$Image['path'].'" width="20%" height="20%" /><div class="btn btn-warning sys-img-edit" style="margin-left:5px;">Удалить</div><br></div>';
					}
				}
				?>
				</div>
				<input class="form-control" id="img" name="img[]" type="file" enctype="multipart/form-data" multiple="multiple"/>
	 		</div>
	 		<div class='form-actions'>
				<input class="btn btn-primary btn-md" name="commit" type="submit" value="Сохранить" />
			</div>
	 </form>
</div>