<div class="col-md-12" style="display:inline-block">
    <div class="alert alert-notice error-search sys-error-search" style='display:none'>
    <i> Пожалуйста заполните все поля  </i>
    </div>
	<div style="text-align: center">

        <div style="font: 400% serif;">
        	<i>
        	Журнал
        	</i>
        </div>
    </div>
    
    <div class="col-md-3">
        <label>Отделы</label>
        <select class="form-control" name="divisions">
        <?
            $div_id = 0;
            if(count($allData) > 0 ) {
                $div_id = $allData[0]['id'];
            }
            foreach($allData as $Division){
                echo '<option value="'.$Division['id'].'">'.$Division['name'].'</option>';
            }
        ?>
        </select>
        
    </div>
  
    <div class="col-md-3">
        <label>Отделения</label>
        <select class="form-control" name="divisionBranchs">
            <?
                $div_brc_id = 0;
                if(count($allData[0]['DivisionBranchs']) > 0) {
                    $div_brc_id = $allData[0]['DivisionBranchs'][0]['id'];
                }
                foreach($allData[0]['DivisionBranchs'] as $DivisionBranch) {
                    echo '<option value="'.$DivisionBranch['id'].'">'.$DivisionBranch['name'].'</option>';
                }
            ?>
        </select>
    </div>

    <div class="col-md-2">
        <label>Журналы</label>
        <select class="form-control" name="journals">
        <?
            foreach ($this->Helpers->Helper_journal->getListJournal($div_id, $div_brc_id) as $Journal) {
                 echo '<option value="'.$Journal['id'].'">'.$Journal['date_begin'].'</option>';
            }
        ?>
        </select>
    </div>

   <div class="col-md-3">
   
           <label>Месяц</label>
            <select class="form-control" name="date">
            <option value="1">Январь</option>
            <option value="2">Февраль</option>
            <option value="3">Март</option>
            <option value="4">Апрель</option>
            <option value="5">Май</option>
            <option value="6">Июнь</option>
            <option value="7">Июль</option>
            <option value="8">Август</option>
            <option value="9">Сентябрь</option>
            <option value="10">Октябрь</option>
            <option value="11">Ноябрь</option>
            <option value="12">Декабрь</option>
        </select>
   </div>
   <div class="col-md-1  sys-submit-search">
       <label>&nbsp;</label>
       <div class="form-control btn btn-default btn-primary">
           <div class="glyphicon glyphicon-search">
           </div>
       </div>
   </div>
</div>

<div class="row">
    <div class="col-md-12">
    
    </div>
    <div class="col-md-12">
    
	<div class="table-responsive" style="margin-top: 50px">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Личный состав</th>
            <th>Заголовок таблицы</th>

          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>Ячейка</td>
          </tr>
        </tbody>
      </table>
    </div>
    </div>
</div>