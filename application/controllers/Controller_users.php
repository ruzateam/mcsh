<?php 

class Controller_users extends Controller {

    public function Action_index($status) {
        $UsersArray = array();
        $i = 0;
        foreach(ORM::for_table('Users')->find_result_set() as $User){
            $UsersArray[$i]['id'] = $User->id;
            $UsersArray[$i]['login'] = $User->login;
            $UsersArray[$i]['avatar'] = $User->avatar;
            $UsersArray[$i]['name'] = $User->name;
            $UsersArray[$i]['secondName'] = $User->second_name;
            $UsersArray[$i]['lastName'] = $User->last_name;
            $UsersArray[$i]['missionLogin'] = $User->misiion_login;
            $UsersArray[$i]['lastIp'] = $User->last_ip;
            $role = ORM::for_table('Groups') -> select('str_role') -> where('int_role', $User->role_id) -> find_one();
            $UsersArray[$i]['role'] = $role -> str_role;
            $i++;
        }
        
        $users = $this->View->loadTemplate('users/index', array('UsersArray' => $UsersArray, 'status'=>$status), false);
        $this->View->runTemplate('template_view', array('body'=>$users));

    }


    public function Action_new() {
        $groups = array();
        $i = 0;
        foreach (ORM::for_table('Groups') ->find_result_set() as $Group) {
            $groups[$i]['id'] = $Group->id;
            $groups[$i]['strRole'] = $Group->str_role;
            $groups[$i]['intRole'] = $Group->int_role;
            $i++;
        }
        $users = $this->View->loadTemplate('users/_new', array('groups' => $groups), false);
        $this->View->runTemplate('template_view', array('body'=>$users));
    }
    
    public function Action_edit() {

        $groups = array();
        $i = 0;
        $user = NULL;

        foreach (ORM::for_table('Groups') ->find_result_set() as $Group) {
            $groups[$i]['id'] = $Group->id;
            $groups[$i]['strRole'] = $Group->str_role;
            $groups[$i]['intRole'] = $Group->int_role;
            $i++;
        }
        
        $user_id = $this->Configs->Args[0];
        $user = ORM::for_table('Users')->where('id',$user_id)->find_one();

        $users = $this->View->loadTemplate('users/_edit', array('groups' => $groups, 'user' => $user), false);
        $this->View->runTemplate('template_view', array('body'=>$users));
    }

    //TODO
    public function Action_create() {
        if(!empty($this->Input->POST)){
            $User = ORM::for_table('Users')->create();
            $User->login = $this->Input->POST['login'];
            $User->name = $this->Input->POST['name'];
            $User->second_name = $this->Input->POST['second_name'];
            $User->last_name = $this->Input->POST['last_name'];
            $User->role_id = $this->Input->POST['role'];
            $User->password = md5($this->Input->POST['password']);
            $User->avatar = $this->loadImages();
			
            $User->save();
        }
        $this->View->fakeRedirect('users', true); 
    }
	
	private function loadImages() {
		if(!empty($_FILES)){
		$UploadFiles = $_FILES;

			$tmpFilePath = $_FILES['avatar']['tmp_name'];
			if ($tmpFilePath != ""){

			  $newFilePath = "application/uploads/" . md5($_FILES['avatar']['tmp_name']).'.jpg';
				
			  if(move_uploaded_file($tmpFilePath, $newFilePath)) {
				return md5($_FILES['avatar']['tmp_name']);
			  }
			  else {
				return '';
			  }
			}
		  
		}	  
	}
	
    public function Action_update() {
        $user = array();
        if(!empty($this->Input->POST['user_id'])){
			$User = ORM::for_table('Users')->where('id',$this->Input->POST['user_id'])->find_one();
			if($User !== false) {
				$User->login = $this->Input->POST['login'];
				$User->name = $this->Input->POST['name'];
				$User->second_name = $this->Input->POST['second_name'];
				$User->last_name = $this->Input->POST['last_name'];
				$User->role_id = $this->Input->POST['role'];
				$User->password = md5($this->Input->POST['password']);
				if(!empty($_FILES)){
					$User->avatar = $this->loadImages();
				}

				$User->save();
			}
		}
		$this->View->fakeRedirect('users', true);

    }

    public function Action_delete() {
        $status = false;
        if(!empty($this->Input->POST['id'])){
            $Id = $this->Input->POST['id'];
            $News = ORM::for_table('Users')->where('id',$Id)->find_one();
            if($News !== false){ 
                $News->delete();
                $status = true; 
            }
        }
        echo json_encode(array('status' => $status), JSON_UNESCAPED_UNICODE);
    }

	public function Action_index_ajax () {
    $UsersArray = array();
        $i = 0;
        foreach(ORM::for_table('Users')->find_result_set() as $User){
            $UsersArray[$i]['id'] = $User->id;
            $UsersArray[$i]['login'] = $User->login;
            $UsersArray[$i]['avatar'] = $User->avatar;
            $UsersArray[$i]['name'] = $User->name;
            $UsersArray[$i]['secondName'] = $User->second_name;
            $UsersArray[$i]['lastLogin'] = $User->last_login;
            $UsersArray[$i]['missionLogin'] = $User->misiion_login;
            $UsersArray[$i]['lastIp'] = $User->last_ip;
            $UsersArray[$i]['roleId'] = $User->role_id;

            $i++;
        }
        
        echo json_encode($UsersArray); 
	}


}