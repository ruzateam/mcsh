<?php

class Controller_journal extends Controller {
    private $coreJornal;
    private $Journals;
    private $allData;
    
    public function Action_index () {
        $this->coreJournal = new coreJournal;
        $allData = $this->coreJournal->loadAllStruct();
        
        $Journal = $this->View->loadTemplate('journal/index', array('allData'=>$allData), false);
        $this->View->runTemplate('template_view', array('body'=>$Journal));
    }

    public function Action_divisions_branch_by_division() {
        $division_id = $this -> Input -> POST['id'];
        $DivisionBranchs = array();
        $Journals = array();
        $i=0;
        foreach(ORM::for_table('DivisionBranch')->where('division_id',$division_id)->find_result_set() as $DivisionBranch) {
            $DivisionBranchs[$i]['id'] = $DivisionBranch->id;    
            $DivisionBranchs[$i]['name'] = $DivisionBranch->name;
            $i++;
        }

        if($i > 0) {
            $j = 0;
            foreach(ORM::for_table('Journals')->where('division_id', $division_id ) -> where('division_branch_id', $DivisionBranchs[0]['id']) ->find_result_set() as $Journal) {
                $Journals[$j]['id'] = $Journal->id;    
                $Journals[$j]['date_begin'] = $Journal->date_begin;
                $Journals[$j]['devision_id'] = $Journal->division_id;
                $Journals[$j]['division_branch_id'] = $Journal->division_branch_id;
                $Journals[$j]['master_division_id'] = $Journal->master_division_id;
                $j++;
            }
        }
        
        echo json_encode(array('count'=>$i, 'division_branchs' => $DivisionBranchs, 'journals' => $Journals), JSON_UNESCAPED_UNICODE);
    }

    public function Action_change_division_branch() {

        $division_id = $this -> Input -> POST['division_id'];
        $division_branch_id = $this -> Input -> POST['division_branch_id'];
        $Journals = array();         
        $i=0;
        foreach(ORM::for_table('Journals')->where('division_id', $division_id ) -> where('division_branch_id', $division_branch_id) ->find_result_set() as $Journal) {

            $Journals[$i]['id'] = $Journal->id;    
            $Journals[$i]['date_begin'] = $Journal->date_begin;
            $Journals[$i]['devision_id'] = $Journal->division_id;
            $Journals[$i]['division_branch_id'] = $Journal->division_branch_id;
            $Journals[$i]['master_division_id'] = $Journal->master_division_id;

            $i++;
        }

        echo json_encode(array('count' => $i, 'journals' => $Journals), JSON_UNESCAPED_UNICODE);
    }


    public function Action_load_journal() {
        $div_id =   $this -> Input -> POST['div_id'];
        $div_brc_id = $this -> Input -> POST['div_brc_id'];
        $journal_id = $this -> Input -> POST['journal_id'];
        $month = $this -> Input -> POST['month'];
        $ok = 'ok';
        echo json_encode(array('status' => $ok), JSON_UNESCAPED_UNICODE);
    }
}

class coreJournal {
    
    private $Divisions;
    
    public function loadAllStruct() {
        $i=0;
        foreach(ORM::for_table('Division')->find_result_set() as $Division){        
            $this -> Divisions[$i]['id']               =   $Division -> id;
            $this -> Divisions[$i]['name']             =   $Division -> name;
            $this -> Divisions[$i]['shortName']        =   $Division -> short_name;
            $this -> Divisions[$i]['DivisionBranchs']  =   $this -> loadDivisionBranch($Division -> id);
            $i++;
        }
        
        //echo(json_encode($this->Divisions, JSON_UNESCAPED_UNICODE));
        return $this->Divisions;
    }
    
    public function showJournals($idDivision=0, $idDivisionBranch=0) {
        
    }
    
    private function loadDivisionBranch($idDivision = 0) {
        $DivisionBranchs = array();
        $i=0;
        foreach(ORM::for_table('DivisionBranch')->where('division_id',$idDivision)->find_result_set() as $DivisionBranch){
            $DivisionBranchs[$i]['id'] = $DivisionBranch->id;    
            $DivisionBranchs[$i]['name'] = $DivisionBranch->name;
            $DivisionBranchs[$i]['masters'] = $this->loadMastersDivisionBranch($idDivision);
            $DivisionBranchs[$i]['Personal'] = $this->loadPersonnelDivisionBranch($idDivision);
            $i++;
        }
        return $DivisionBranchs;
    }
    
    private function loadMastersDivisionBranch($idDivision){
        $Masters = array();
        $i=0;
        foreach(ORM::for_table('Masters')->where('division_id',$idDivision)->find_result_set() as $Master){
            $Masters[$i]['id']              = $Master->id;    
            $Masters[$i]['name']            = $Master->name;
            $Masters[$i]['second_name']     = $Master->second_name;
            $Masters[$i]['last_name']       = $Master->last_name;
            $Masters[$i]['post']            = $Master->post;
            $Masters[$i]['range']           = $this->loadRangeById($Master->range_id);
            $i++;
        }
        return $Masters;
    }
    
    private function loadRangeById($idRange = 0){
        if(empty($idRange))
        return false;
    
        $Range = ORM::for_table('Range')->where('id',$idRange)->find_one();
        return $Range->full_name;
    }
    
    private function loadPersonnelDivisionBranch($idDivisionBranch) {
        $People = array();
        $i=0;
        foreach(ORM::for_table('Personal')->where('division_branch_id',$idDivisionBranch)->find_result_set() as $Person){
            $People[$i]['id'] = $Person->id;
            $People[$i]['name'] = $Person->name;
            $People[$i]['second_name'] = $Person->second_name;
            $People[$i]['last_name'] = $Person->last_name;
            $People[$i]['range']     = $this->loadRangeById($Person->range_id);
            $i++;
        }
        return $People;
    }
}

