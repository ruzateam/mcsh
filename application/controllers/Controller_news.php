<?php

class Controller_news extends Controller {
    
	private $news = array();


    public function Action_index () {
        $news = $this->View->loadTemplate('news/index', array(), false);
        $this->View->runTemplate('template_view', array('body'=>$news));
    }

	public function Action_new () {
		$news = $this->View->loadTemplate('news/_new', array(), false);
        $this->View->runTemplate('template_view', array('body'=>$news));
	}

	public function Action_create($arguments = array()) {
		if(!empty($this->Input->POST)){
			$title = $this->Input->POST['title'];
			$body = $this->Input->POST['body'];
			$News = ORM::for_table('News')->create();
			$News->title = $title;
			$News->body = $body;
			$News->set_expr('date', 'NOW()');
			$News->save();
			$this->loadImages($News->id());
		}
        $this->View->fakeRedirect('news', true); 
	}
	
	private function saveEditNews() {
		$Id = $this->Input->POST['news_id'];
		$title = $this->Input->POST['title'];
		$body = $this->Input->POST['body'];
		$News = ORM::for_table('News')->where('id',(int) $Id)->find_one();
			if($News !== false){
				$News->title = $title;
				$News->body = $body;
				$News->save();
				if(!empty($_FILES)){
					$this->loadImages($Id);	
				}
				$this->View->fakeRedirect('news',true);
			}
	}
	
	public function Action_delete() {
		$status = true;
		if(!empty($this->Input->POST['id'])){
			$Id = $this->Input->POST['id'];
			$News = ORM::for_table('News')->where('id',$Id)->find_one();
			if($News !== false){
				$News->delete();
				$status = true;	
			}
			$newsImage = ORM::for_table('Images')->where_equal('news_id',$Id)->find_many();
			if($newsImage !== false){
			//	$newsImage->delete_many();
			}
		}
		echo json_encode(array('status' => $status), JSON_UNESCAPED_UNICODE);
	}

	public function Action_deleteImage () {
		if(!empty($this->Input->POST['id'])) {
			$newsImage = ORM::for_table('Images')->where('id',$this->Input->POST['id'])->find_one();
			if($newsImage !== false){
				$newsImage->delete();
				
			}
			echo json_encode(array('status' => 'true'), JSON_UNESCAPED_UNICODE);
		}
	}
	
	
    public function Action_edit($args) {
		$bodyNews = array('body'=>'','title'=>'','imgs'=>array());
		$ImgsArray = array();
		
		if(!empty($this->Input->POST['news_id'])){
				$this->saveEditNews();
				return 0;
			}
			
		if(!empty($args[0])){

			$Id = $args[0];
			$News = ORM::for_table('News')->where('id',$Id)->find_one();
			if($News !== false){
				$bodyNews['title'] = $News->title;
				$bodyNews['body'] = $News->body;
			}
			
			$Images = ORM::for_table('Images')->where('news_id',$Id)->find_result_set();
			if(!empty($Images)){
				foreach($Images as $Image){
					$ImgsArray[$Image->id]['path'] = BASE_URL.'application/uploads/'.$Image->path.'.jpg';
					$ImgsArray[$Image->id]['name'] = $Image->path;
					$ImgsArray[$Image->id]['image_id'] = $Image->id;
				}
			}	
		$bodyNews['imgs'] = $ImgsArray;
		}
    	$news = $this->View->loadTemplate('news/_edit', array('bodyNews'=>$bodyNews), false);
        $this->View->runTemplate('template_view', array('body'=>$news));
    }

    public function Action_update() {

    }

    private function setParams($argv = array()) {
    	$news['id']  = $argv['id'];
    	$news['title'] = $argv['title'];
    	$news['context'] = $argv['context'];
    }
	
	private function loadImages($idNews) {
		if(!empty($_FILES)){
		$UploadFiles = $_FILES;
		for($i=0; $i<count($_FILES['img']['name']); $i++) {
			$tmpFilePath = $_FILES['img']['tmp_name'][$i];
			if ($tmpFilePath != ""){

			  $newFilePath = "application/uploads/" . md5($_FILES['img']['tmp_name'][$i]).'.jpg';
		  

			  $this->addImageInDB($idNews, md5($_FILES['img']['tmp_name'][$i]));
			  if(move_uploaded_file($tmpFilePath, $newFilePath)) {

			  }
			}
		  }
		}	  
	}
	
	private function addImageInDB ($idNews, $Path) {
		$Image = ORM::for_table('Images')->create();
		$Image->path = $Path;
		$Image->news_id = $idNews;
		$Image->save();
	}
   
}