<?php

class Controller_workbench extends Controller {
    
    
    public function Action_index() {
        
        
        $workBench = $this->View->loadTemplate('workbench/index', array(), false);
        $this->View->runTemplate('template_view', array('body'=>$workBench));
    }
    

    public function Action_division() {
        $workBench = $this->View->loadTemplate('workbench/index_division', array(), false);
        $this->View->runTemplate('template_view', array('body'=>$workBench));
    }

    public function Action_division_bench() {
        $workBench = $this->View->loadTemplate('workbench/index_division_bench', array(), false);
        $this->View->runTemplate('template_view', array('body'=>$workBench));
    }

    public function Action_personal() {
        $workBench = $this->View->loadTemplate('workbench/index_personal', array(), false);
        $this->View->runTemplate('template_view', array('body'=>$workBench));
    }

    public function Action_journal() {  
        $workBench = $this->View->loadTemplate('workbench/index_journal', array(), false);
        $this->View->runTemplate('template_view', array('body'=>$workBench));
    }

    public function Action_show_divisions() {
        $Divisions = ORM::for_table('Division')->find_result_set();
        $DivisionsArray = array();
        $i=0;

        foreach($Divisions as $Division){
            $DivisionsArray[$i][] = $Division->id;
            $DivisionsArray[$i][] = $Division->name;
            $DivisionsArray[$i][] = $Division->short_name;
            $i++;
        }
        
        //echo json_encode(array('data'=>$DivisionsArray,'heads'=>$FieldName), JSON_UNESCAPED_UNICODE);
        echo json_encode(array('data'=>$DivisionsArray), JSON_UNESCAPED_UNICODE);
       
       
    }
    
    
    public function Action_get_header(){
        $FieldName = array();
        $FieldName['showDivivsions'] = array('id','Имя отдела','Краткое название отдела');
        echo json_encode($FieldName['showDivivsions'], JSON_UNESCAPED_UNICODE);
    }
    
    public function Action_show_division_branch() {
        $DivisionBranchs = ORM::for_table('DivisionBranch')->find_result_set();
        $DivisionBranchArray = array();
        $i=0;

        foreach($DivisionBranchs as $DivisionBranch) {
            $DivisionBranchArray[$i][] = $DivisionBranch->id;
            $DivisionBranchArray[$i][] = $DivisionBranch->division_id;
            $DivisionBranchArray[$i][] = $DivisionBranch->name;
            $i++;
        }
        
        echo json_encode(array('data'=>$DivisionBranchArray), JSON_UNESCAPED_UNICODE);
    }

        public function Action_ajax_division_branch($CRUD) {
        switch($CRUD[0]){
            case 'delete': 
                $this->deleteDivisionBranch($this->Input->POST['id']); 
                break;
            case 'update':
                $this->updateDivisionBranch($this->Input->POST); 
                break;
        }
    }

    private function deleteDivisionBranch() {
        $DivisionBranch = ORM::for_table('DivisionBranch')->where('id',$id)->find_one();
        if($DivisionBranch != false);
            $DivisionBranch->delete();
    }

    private function updateDivisionBranch($Data) {
       $DivisionBranch = ORM::for_table('DivisionBranch')->where('id',$Data['id'])->find_one();
            if($DivisionBranch != false) {

                if($Data['columnId'] == '0'){
                    echo 'Нельзя править id';
                    return false;
                }
                if($Data['columnId'] == '1')
                    $DivisionBranch->division_id = $Data['value'];
                if($Data['columnId'] == '2')
                    $DivisionBranch->name = $Data['value'];
                 $q = $DivisionBranch->save(); 
                echo $Data['value'];
            } 
    }

    public function Action_show_personal() {
        $Personal = ORM::for_table('Personal')->find_result_set();
        $PersonalArray = array();
        $i=0;

        foreach($Personal as $Person){
            $PersonalArray[$i][] = $Person->id;
            $PersonalArray[$i][] = $Person->division_branch_id;
            $PersonalArray[$i][] = $Person->name;
            $PersonalArray[$i][] = $Person->second_name;
            $PersonalArray[$i][] = $Person->last_name;
            $PersonalArray[$i][] = $Person->range_id;

            $i++;
        }
        echo json_encode(array('data'=>$PersonalArray), JSON_UNESCAPED_UNICODE);
    }

    public function Action_show_journals() {
        $Journals = ORM::for_table('Journals')->find_result_set();
        $JournalsArray = array();
        $i=0;

        foreach($Journals as $Journal){
            $JournalsArray[$i][] = $Journal->id;
            $JournalsArray[$i][] = $Journal->date_begin;
            $JournalsArray[$i][] = $Journal->division_id;
            $JournalsArray[$i][] = $Journal->division_branch_id;
            $JournalsArray[$i][] = $Journal->master_division_id;
            $i++;
        }
        echo json_encode(array('data' => $JournalsArray), JSON_UNESCAPED_UNICODE);
    }


    public function Action_ajax_journal($CRUD) {
        switch($CRUD[0]){
            case 'delete': $this->deleteJournal($this->Input->POST['id']); break;
            case 'update': $this->updateJournal($this->Input->POST); 
            break;
        }
    }

    private function deleteJournal($id) {
        $Journal = ORM::for_table('Journals')->where('id',$id)->find_one();
        if($Journal != false);
            $Journal->delete();
    }

    private function updateJournal($Data) {
       $Journal = ORM::for_table('Personal')->where('id',$Data['id'])->find_one();
        if($Journal != false) {

            if($Data['columnId'] == '0'){
                echo 'Нельзя править id';
                return false;
            }
            if($Data['columnId'] == '1')
                $Journal->division_branch_id = $Data['value'];
            if($Data['columnId'] == '2')
                $Journal->name = $Data['value'];
            if($Data['columnId'] == '3')
                $Person->second_name = $Data['value'];
            if($Data['columnId'] == '4')
                $Person->last_name = $Data['value'];
            if($Data['columnId'] == '5')
                $Person->range_id = $Data['value'];
            $Person->save(); 
            echo $Data['value'];
        } 
    }


    public function Action_ajax_personal($CRUD) {
        switch($CRUD[0]){
            case 'delete': $this->deletePerson($this->Input->POST['id']); break;
            case 'update': $this->updatePerson($this->Input->POST); 
            break;
        }
    }

    private function deletePerson($id) {
        $Person = ORM::for_table('Personal')->where('id',$id)->find_one();
        if($Person != false);
            $Person->delete();
    }

    private function updatePerson($Data) {
        $Person = ORM::for_table('Personal')->where('id',$Data['id'])->find_one();
            if($Person != false) {

                if($Data['columnId'] == '0'){
                    echo 'Нельзя править id';
                    return false;
                }
                if($Data['columnId'] == '1')
                    $Person->division_branch_id = $Data['value'];
                if($Data['columnId'] == '2')
                    $Person->name = $Data['value'];
                if($Data['columnId'] == '3')
                    $Person->second_name = $Data['value'];
                if($Data['columnId'] == '4')
                    $Person->last_name = $Data['value'];
                if($Data['columnId'] == '5')
                    $Person->range_id = $Data['value'];
                $Person->save(); 
                echo $Data['value'];
            } 
    }

    public function Action_ajax_divisions($CRUD) {
        switch($CRUD[0]){
            case 'delete': $this->deleteDivision($this->Input->POST['id']); break;
            case 'update': $this->updateDivision($this->Input->POST); break;
        }
        
       //logs($this->Input->POST);
        
    }
    


    private function deleteDivision($id){
        $Division = ORM::for_table('Division')->where('id',$id)->find_one();
        if($Division != false);
            $Division->delete();
    }

    private function updateDivision($Data){
        //Logs($Data);
        
        $Division = ORM::for_table('Division')->where('id',$Data['id'])->find_one();
            if($Division != false) {

                if($Data['columnId'] == '0'){
                    echo 'Нельзя править id';
                    return false;
                }
                if($Data['columnId'] == '1')
                    $Division->name = $Data['value'];
                if($Data['columnId'] == '2')
                    $Division->short_name = $Data['value'];
                $Division->save(); 
                echo $Data['value'];
            } 
    }
}
