<?php

class Controller_login extends Controller {
    
    private $Model = null; 
    
    public function Action_index () {
        $this->loadModel();
        $data = array('message'=>'');
        if(!empty($this->Input->POST)){
            $idUser = $this->Validation();
            if($idUser !== Null) {
                $this->setSession($idUser);
                $this->View->fakeRedirect('news');
            } else {
                $data['message'] = 'Не правильный логин или пароль';
            }
        }
        $this->View->runTemplate('login_view',$data);
    }
    
    public function Action_logout () {
        if(!empty($this->Library->Session->getInSession('user_id')))
            $this->Library->Session->sessionDestroy();
        $this->View->fakeRedirect('login', true); 
    }
    
    private function loadModel () {
        include_once 'application/models/' . $this->Configs->Controller_name . '.php';
        $this->Model = new Model_login();
    }
    
    private function Validation() {
        if(!empty($this->Input->POST['login']) AND !empty($this->Input->POST['password'])){
            $check = $this->Model->validationLogin($this->Input->POST);
            return $check;
        }
    }
    
    private function setSession($idUser) {
        $this->Library->Session->sessionStart();
        $this->Library->Session->setInSession(array('user_id'=>$idUser));
        $this->Library->ACS->loadUserInfo($idUser);
        $this->addUserInfo($idUser);
    }
    
    private function addUserInfo ($idUser) {
        $User = ORM::for_table('Users')->where('id',$idUser)->find_one();
        $User->last_ip = $this->Library->Session->showUserInfo('ip');
        $User->save();
    }
}