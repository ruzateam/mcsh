<?php

class Controller_ajax extends Controller {
    
    
    public function Action_index($array) {

    }
    
    public function Action_ajax () {
        
    }
    
    public function Action_news($arguments = array()) {

        $argv = $this->Input->POST;
        $page = (int) $argv['page'];
        $count = (int) $argv['count'];
        $count_news = ORM::for_table('News') -> count();

        if(($page < 0) OR ($count < 0) or $count_news < $page * $count) {
            echo '[]';
        } else {
        
        $NewsArray = array();
        $i = 0;
        foreach(ORM::for_table('News')->limit($page*$count)->offset($page*$count - $count)->order_by_desc('date')->find_result_set() as $New){
            $NewsArray[$i]['id'] = $New->id;
            $NewsArray[$i]['date'] = $New->date;
            $NewsArray[$i]['title'] = $New->title;
            $NewsArray[$i]['body'] = $New->body;
            foreach(ORM::for_table('Images')->where('news_id',$New->id)->find_result_set() as $Img) {
                $NewsArray[$i]['img'][] = BASE_URL.'application/uploads/'.$Img->path.'.jpg';
            }
            $i++;
        }
        
        echo json_encode($NewsArray);   
        
        }
    }
}   