<?php

class View {
    
    private $OUTPUT = '';
    public $Configs;
    public $Library;
    public $User;
    public $Helpers;
    
    function __construct($Configs, $Library){
        $this->Configs = $Configs;
        $this->Library = $Library;
        $this -> Helpers = new Helpers;
        $this->load_helpers();
    }
    
    public function runTemplate($view, $data = array()) {
        $this->loadTemplate($view, $data)->show();
        
    }
    
    public function Redirect($url = '') {
            header("Location: ".BASE_URL.$url);
    }
    
    public function fakeRedirect($url = '', $start = false) {
        $script = "
        <script>
            window.location.href = '".BASE_URL.$url."';
        </script>";
        
        if($start){
            echo $script;
        } else {
            $this->OUTPUT .= $script;
        }
    }
    
    public function loadTemplate($view, $data = array(), $display = true) {
        $this->User = $this->Library->ACS->getUserInfo();
        $path = 'application/views/' . $view . '.php';
        if (!file_exists($path)) {
            echo 'Шаблон ' . $view . ' не найден';
            return false;
        }
        
        ob_start();
        if (!empty($data))
            extract($data);
        $User = new User;
        $tokenUser = new tokenUser;
        include $path;
        $buffer = ob_get_contents();
 
        ob_implicit_flush(0);
        ob_end_clean();
        
        if($display){
            $this->OUTPUT .= $buffer;
            return $this;
        }
        else {
            
            return $buffer;
        }
    }

    public function load_helpers() {

        foreach ($this->Configs->Helpers as $name_helper) {
             $path = 'application/helpers/' . $name_helper . '.php';

            if (!file_exists($path)) {
                echo 'Хелпер ' . $name_helper . ' не найден';
                return false;
            }
            include $path;
            $this -> Helpers -> $name_helper = new $name_helper;
        }
    }

    public function show() {
        if (empty($this->OUTPUT)) {
            return FALSE;
        }
        echo $this->OUTPUT;
        $this->OUTPUT = '';
        return $this;
    }
}

class tokenUser {
    public $Action;
    public $baseUrl;
    function __construct () {
        $this->Action  = explode('/', $_SERVER['REQUEST_URI']);
        $this->Action  = $this->Action[1];
        $this->baseUrl = BASE_URL;
    }
    
}

class Helpers {

}