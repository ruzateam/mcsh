<?php

class Engine {

    public $Configs  = null;
    public $Settings = null;
    public $Input    = null;
    public $Library  = null;
    
    function __construct () {
        $this->loadConfigs();
        $this->loadIdiorm();
        $this->loadSettings();
        $this->loadLibrary();
        $this->routeStart();
        $this->inputControl();
        $this->loadController();
    }
    
    
    // Загрузка класса конфигурации
    private function loadConfigs() {
        $this->Configs = new Configs;
    }
    
    // Подключение библиотеки базы данных
    private function loadIdiorm() {
        require_once('idiorm.php');
        ORM::configure("mysql:host=".$this->Configs->IP_DB.";dbname=".$this->Configs->DB_name);
        ORM::configure('username', $this->Configs->Login);
        ORM::configure('password', $this->Configs->Password);
        ORM::configure('driver_options', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
    }
    
    // Загрузка параметров из БД
    private function loadSettings() {
        $this->Settings = new Settings;
        
        foreach(ORM::for_table('Settings')->find_result_set() as $record) {
            $Name = $record->name;
            $Setting = $record->setting;
            $this->Settings->$Name = $Setting;
        }
        
    }
    
    //Подгрузка библиотек
    private function loadLibrary () {
        $this->Library = new Library;
        foreach ($this->Configs->Library as $Library){
            if (file_exists('application/library/' . $Library . '.php')) {
                include 'application/library/' . $Library . '.php';
                $this->Library->$Library = new $Library;
            } else {
                echo 'Библиотека '.$Library.' не найдена';
            }
        } 
    }
    
    // Роутинг
    private function routeStart() {
        $this->detectURL();
    }
    
    // Дедектирование URL
    private function detectURL(){    
        $URL =  explode('/', $_SERVER['REQUEST_URI']);
        $this->Configs->URL = $URL;
        $this->Configs->Args = array_slice($URL,3);

     
        if(!empty($URL[1])){
            $this->Configs->Controller_name = $URL[1];
        } else {
            $this->Configs->Controller_name = 'news';
        }

        $this->Configs->Controller = 'Controller_'.$this->Configs->Controller_name;
        
         
        if(!empty($URL[2])){
            $this->Configs->Action = $URL[2];
        } else {
            $this->Configs->Action = 'index';
        }

    }
    
    //Формировка поступающих данных POST GET DELETE PUT
   private function inputControl () {
    $this->Input = new Input;
        if(!empty($_POST)) {
            $this->Input->POST = $_POST;
        } else {
            $this->Input->POST = array();
        }
        if(!empty($_GET)) {
            $this->Input->GET = $_GET;
        } else {
            $this->Input->GET = array();
        }   
   }
   
   // Загрузка контроллера
   private function loadController() {
        
        if($this->Configs->Controller_name != 'login'){
            if(!$this->Library->ACS->checkAuth()){
                $this->Configs->Controller_name = 'login';
                $this->Configs->Controller = 'Controller_login';
            }
        }
        
        if (!file_exists('application/controllers/' . $this->Configs->Controller . '.php')) {
            $this->Configs->Controller = 'Controller_404';
            $this->Configs->Controller_name = '404';
        } 
        
            include 'application/controllers/' . $this->Configs->Controller . '.php';
            $Controller = new $this->Configs->Controller($this->Configs,
                                                         $this->Settings,
                                                         $this->Input,
                                                         $this->Library);
            
            $action = 'Action_'.$this->Configs->Action;
            $Controller->$action($this->Configs->Args);
   }
   
}

class Settings {
    
}

class Library {
    
}

class Input {
    
}
