<?php

class Session {
    
    
    private $UserInfo = null;
    
    
    function __construct () {
        $this->UserInfo = new UserInfo;
        $this->getUserInfo();
    }
    

    
    public function sessionStart() {
        $Session = ORM::for_table('Sessions')->create();
        $Session->session_id = $this->UserInfo->session_id;
        $Session->user_ip = $this->UserInfo->ip;
        $Session->set_expr('date', 'NOW()');
        $Session->save();
    }
    
    public function sessionDestroy() {
        session_destroy();
    }
    
    public function setInSession ($argv = array()) {
        if(!empty($argv)){
            if(is_array($argv)){
                foreach($argv as $key=>$value){
                    $_SESSION[$key] = $value;
                }
                return TRUE;
            }
        } else {
            return FALSE;
        }
    }
    
    public function getInSession ($key = '') {
        if(!empty($key)){
            if($_SESSION[$key] !== NULL){
                return $_SESSION[$key];
            } else {
                return FALSE;
            }
        }
    }
    
    public function showUserInfo ($key = '') {
        if(empty($key))
            return $this->UserInfo;
        else
            return $this->UserInfo->$key;
    }
    
    private function getUserInfo () {
        $this->UserInfo->ip = $_SERVER['REMOTE_ADDR'];
        $this->UserInfo->session_id = session_id();
    }
}

class UserInfo {
    public $ip;
    public $session_id;
}