<?php

class ACS {
 
    public $User;
    
    function __construct () {
        $this->User = new User();
        $this->loadUserInfo($_SESSION['user_id']);
    }
    
    public function checkAuth () {
        if(!isset($_SESSION['user_id'])){
            return FALSE;
        }
        else{
            return TRUE;
        }
    }
    
    public function loadUserInfo ($idUser) {
        $User = ORM::for_table('Users')->where('id',$idUser)->find_one();
        $Group = ORM::for_table('Groups')->where('int_role',$User->role_id)->find_one();
        $this->User->setUser($User,$Group);
    }

    public function getUserInfo() {
        return $this->User;
    }
    
    public function checkPerm($key){
        
        
        if(!empty($this->User->perms[$key])){
            return true;
        } else {
            return false;
        }
    }

}

class User {
    public $login;
    public $avatar;
    public $user_id;
    public $str_role;
    public $int_role;
    public $perms;
    
    public function setUser($User,$Group) {
        $this->login = $User->login;   
        $this->user_id = $User->id;      
        $this->avatar = $User->avatar;      
        $this->str_role = $Group->str_role;      
        $this->int_role = $Group->int_role;
        $this->perms = $this->loadPerms(json_decode($Group->perms_id));
    }
    
    private function loadPerms($ids) {
        $permsArray = array();
            $perms = ORM::for_table('Permissions')->where_in('id',$ids)->find_many();
            foreach($perms as $perm){
            $permsArray[$perm->key] = true;
            }
            return $permsArray;
    }
}