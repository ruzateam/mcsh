<?php 

class Jounral extends Model {

	private $id;
	private $dateBegin;
	private $divisionId;
	private $divisionBranchId;
	private $masterDivisionId;

	public function setId($id) {
		$this -> id = $id;
	}

	public function getId() {
		return $this -> id;
	}

	public function setDateBegin($dateBegin) {
		$this -> dateBegin = $dateBegin;
	}

	public function getDateBegin() {
		return $this -> dateBegin;
	}

	public function setDivisionId($divisionId) {
		$this -> divisionId = $divisionId;
	}

	public function getDivivsionId() {
		return $this -> divisionId;
	}

	public function setDivisionBranchId($divisionBranchId) {
		$this -> divisionBranchId = $divisionBranchId;
	}

	public function getDivivsionId() {
		return $this -> divisionBranchId;
	}

	public function setMasterDivisionId($masterDivisionId) {
		$this -> masterDivisionId = $masterDivisionId;
	}

	public function getMasterDivisionId() {
		return $this -> masterDivisionId;
	}

}
