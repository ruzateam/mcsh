<?php 

class Group extends Model {
	
	private $id;
	private $strRole;
	private $intRole;

	public function setId($id) {
		$this -> id = $id;
	}

	public function getId() {
		return $this -> id;
	}

	public function setStrRole($strRole) {
		$this -> strRole = $strRole;
	}

	public function getStrRole() {
		return $this -> strRole;
	}

	public function setIntRole($intRole) {
		$this -> intRole = $intRole;
	}

	public function getIntRole() {
		return $this -> intRole;
	}
}