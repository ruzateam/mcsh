<?php 

class Master extends Model {

	private $id;
	private $divisionId;
	private $post;
	private $rangeId;
	private $name;
	private $secondName;
	private $lastName;

	public function setId($id) {
		$this -> id = $id;
	}

	public function getId() {
		return $this->id;
	}

	public function setDivisionId($divisionId) {
		$this -> divisionId = $divisionId;
	}

	public function getDivisionId(){
		return $this -> divisionId;
	}

	public function setPost($post) {
		$this -> post  = $post;
	}

	public function getPost() {
		return $this->post;
	}

	public function setRangeId($rangeId) {
		$this -> rangeId = $rangeId;
	}

	public function getRangeid() {
		return $this -> rangeId;
	}

	public function setName($name) {
		$this -> name = $name;
	}

	public function getName() {
		return $this-> name;
	}

	public function setSecondName($secondName) {
		$this -> secondName = $secondName;
	}
	public function getSecondName() {
		return $this -> secondName;
	}

	public function setLastName($lastName) {
		$this -> lastName = $lastName;
	}

	public function getLastName() {
		return $this->lastName;
	}
}