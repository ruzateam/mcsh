<?php 

class Division extends Model {

	private $id;
	private $name;
	private $shortName;

	public function setId($id) {
		$this -> id = $id;
	}

	public function getId() {
		return $this -> id;
	}

	public function setName($name) {
		$this -> name = $name;
	}

	public function getName() {
		return $this -> name;
	}

	public function setShortName($shortName) {
		$this -> shortName = $short_name;
	}

	public function getShortName() {
		return $this -> shortName;
	}

}