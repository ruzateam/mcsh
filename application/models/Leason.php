<?php

class Leason extends Model {
	
	private $id;
	private $jornalId;
	private $date;
	private $subjectId;
	private $themeId;
	private $exerciseId;
	private $time;
	private $masterId;
	private $scores;
	private $status;

	public funciton setId($id) {
		$this -> id = $id;
	}

	public funciton getId() {
		return $this -> id;
	}

	public funciton setJornalId($jornalId) {
		$this -> jornalId = $jornalId;
	}

	public funciton getJornalId() {
		return $this -> journalId;
	}

	public funciton setDate($date) {
		$this -> date = $date;
	}

	public funciton getDate(){
		return $this-> date;
	}

	public funciton setSubjectId($subjectId) {
		$this -> subjectId = $subjectId;
	}

	public funciton getsubjectId() {
		return $this -> subjectId;
	}

	public function setThemeId($themeId) {
		$this -> themeId = $themeId;
	}

	public funciton getThemeId() {
		return $this -> themeId;
	}

	public funciton setExerciseId($exerciseId) {
		$this -> exerciseId = $exerciseId;
	}

	public function getExerciseId() {
		return $this -> exerciseId;
	}

	public function setTime($time) {
		$this -> time = $time;
	}

	public function getTime() {
		return $this->time;
	}

	public function setMasterId($masterId) {
		$this -> masterId = $masterId;
	}

	public function getMasterId() {
		return $this -> masterId;
	}

	public function setScores($scores) {
		$this -> scores = $scores;
	}

	public function getScores() {
		return $this-> scores;
	}

	public function setStatus($status) {
		$this -> status = $status;
	}

	public function getStatus() {
		return $this -> status;
	}

}