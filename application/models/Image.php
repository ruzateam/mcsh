<?php

class Image extends Model {

	private $id;
	private $newsId;
	private $path;

	public function setId($id) {
		$this -> id = $id;
	}

	public function getId() {
		return $this -> id;
	}

	public function setNewsId($newsId) {
		$this -> newsId = $newsId;
	}

	public function getNewsId() {
		return $this -> newsId;
	}

	public function setPath($path) {
		$this -> path = $path;
	}

	public function getPath() {
		return $this -> path;
	}
} 