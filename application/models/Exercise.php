<<?php 

class Exercise extends Model {

	private $id;
	private $themeId;
	private $number;
	private $name;

	public funciton setId($id) {
		$this -> id = $id;
	}

	public funciton getId() {
		return $this -> id;
	}

	public funciton setThemeId($themeId) {
		$this -> themeId = $themeId;
	}

	public funciton getThemeId() {
		return $this -> themeId;
	}

	public funciton setNumber($number) {
		$this -> number = $number;
	}

	public funciton getNumber() {
		return $this -> number;
	}

	public funciton setName($name) {
		$this -> name = $name;
	}

	public funciton getName() {
		return $this -> name;
	}
}