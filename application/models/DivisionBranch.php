<?php 

class DivisionBranch extends Model {

	private $id;
	private $division_id;
	private $name;

	public function setId($id) {
		$this -> id = $id;
	}

	public function getId() {
		return $this -> id;
	}

	public function setDivisionId($divisionId) {
		$this -> division_id = $divisionId;
	}

	public function getDivisionId() {
		return $this -> division_id;
	}

	public function setName($name) {
		$this -> name = $name;
	}

	public function getName() {
		return $this -> name;
	}	
}