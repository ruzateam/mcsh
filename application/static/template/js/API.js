API = {
	
    //=====================   NEWS ============================
    loadNews : function(params, callback, err) {
       return  API.post('/ajax/news/', params, callback, err);
    },

     deleteNews : function(params, callback, err) {
         return API.post("/news/delete/", params, callback, err);
     },
	 
     deleteImage : function(params, callback, err) {
         return API.post("/news/deleteImage/", params, callback, err);
     },

     //==================== USERS =============================

     deleteUser : function(params, callback, err) {
        return API.post("/users/delete/", params, callback, err);
     },
	 
	 //==================== USERS =============================
	
	getDivisions : function (params, callback, err){
		return API.post("/workbench/show_divisions/", params, callback, err);
	},


    //===================== JOURNAL ==========================
    loadDivisionsBranchsByDivision : function (params, callback, err){
        return API.post("/journal/divisions_branch_by_division/", params, callback, err);
    },


    loadJournal : function(params, callback, err) {
        return API.post("/journal/load_journal", params, callback, err);
    },

    //load json for select journal
    processChangeDivisionBranch: function(params, callback, err) {
        return API.post("/journal/change_division_branch/", params, callback, err);
    },
	
	//=====================  WorkBench  =======================
	
    getHeaderForTable: function(params, callback, err) {
        return API.post("/workbench/get_header/", params, callback, err);
    },

    //=====================  AJAX  ============================

    post: function (url, params, callback, err) {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: url,
            data: params,
            time: 30000,
            success: callback,
            error: err
        });
    },

    get: function (url, params, callback, err) {
        $.ajax({
            type: "GET",
            dataType: "json",
            url: url,
            data: params,
            time: 30000,
            success: callback,
            error: err
        });
    }
}