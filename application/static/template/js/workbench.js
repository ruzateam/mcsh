Workbench = {
  
    init: function () {
        Workbench.init_tables();
    },
    
    init_tables: function() {
      var action = $('.sys-action').attr('value');
      switch(action)  {
        case 'division' :
          $('.sys-button-table').show();
          Workbench.getDivisions();
          break;
        case 'division_bench':
          $('.sys-button-table').show();
          Workbench.getDivisionBranch();  
        case 'personal' : 
          $('.sys-button-table').show();
          Workbench.getPersonal();  
          break;
        case 'journal' :
          $('.sys-button-table').show();
          Workbench.getJournal();
        default: 
          break;
      }
    }, 
      
    getDivisions: function () {

      $('#sys-table-workbench-division').dataTable( {
                    'sAjaxSource': '/workbench/show_divisions/',

                    columns: [
                         { title: "id" },
                         { title: "Название подразделения" },
                         { title: "Краткое название" }
                      ],
          "bScrollCollapse": true,
          "bPaginate": false,
          "bJQueryUI": true
          
        }).makeEditable({
                  sUpdateURL: "/workbench/ajax_divisions/update",
                                  sAddURL: "AddData.php",
                                  sDeleteURL: "test/index/",
                                  sDeleteRowButtonId: "btnDeleteRow"
                    });

    },

    getDivisionBranch : function() {
        $('#sys-table-workbench-division-bench').dataTable( {
                    'sAjaxSource': '/workbench/show_division_branch/',

                    columns: [
                         { title: "id" },
                         { title: "id Отдела" },
                         { title: "Имя"}
                         ],
          "bScrollCollapse": true,
          "bPaginate": false,
          "bJQueryUI": true
          
        }).makeEditable({
                  sUpdateURL: "/workbench/ajax_division_branch/update",
                                  sAddURL: "AddData.php",
                                  sDeleteURL: "test/index/",
                                  sDeleteRowButtonId: "btnDeleteRow"
                    });
    },

    getPersonal : function() {
        $('#sys-table-workbench-personal').dataTable( {
                    'sAjaxSource': '/workbench/show_personal/',

                    columns: [
                         { title: "id" },
                         { title: "Отделение" },
                         { title: "Имя" },
                         { title: "Фамилия" },
                         { title: "Отчество" },
                         { title: "Звание" }

                      ],
          "bScrollCollapse": true,
          "bPaginate": false,
          "bJQueryUI": true
          
        }).makeEditable({
                  sUpdateURL: "/workbench/ajax_personal/update",
                                  sAddURL: "AddData.php",
                                  sDeleteURL: "test/index/",
                                  sDeleteRowButtonId: "btnDeleteRow"
                    });
    },

    getJournal: function() {

      $('#sys-table-workbench-journal').dataTable( {
                    'sAjaxSource': '/workbench/show_journals/',

                    columns: [
                         { title: "id" },
                         { title: "Начало введения" },
                         { title: "id отдела" }, 
                         { title: "id отделения" },
                         { title: "id Инструктирующего" }


                      ],
          "bScrollCollapse": true,
          "bPaginate": false,
          "bJQueryUI": true
          
        }).makeEditable({
                  sUpdateURL: "/workbench/ajax_journal/update",
                                  sAddURL: "AddData.php",
                                  sDeleteURL: "test/index/",
                                  sDeleteRowButtonId: "btnDeleteRow"
                    });
    }
        
    
};