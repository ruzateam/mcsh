MCHS = {

	init: function() {
		var controller = $('input[name=sys-controller]').attr('value');
		MCHS.listener_menu();
		switch(controller) {
			case 'users' :
				MCHS.init_users();
				break;
			case 'news' :
				MCHS.init_news();
				MCHS.onclikerDeleteImage();
				break;
			case 'journal':
				MCHS.init_journal();
				break;
			case 'workbench' : 
				Workbench.init();
				break;
			case 'group_perms' : 
				MCHS.group_perms();
				break;
		}
	},

	// ======================== NEWS =======================//
	init_news: function() {
		var action = $('input[name=sys-action]').attr('value'); 
		switch(action) {
			case 'index' :
				MCHS.loadNews();
				MCHS.scrollNews();
				break;
		}
	},

	loadNews : function() {
		var page = parseInt($('input[name=page]').attr('value'));
		var count = $('input[name=count]').attr('value');
		$('input[name=page]').attr('value', (page + 1));
		API.loadNews({'page': page, 'count':count}, function(data) {
			var item_orign = $(document).find(".sys-news-item-hidden");
			if(data.length == 0) {
				MCHS.dontScrollNews();
			}
			$.each(data, function(index_news) {
				var item = item_orign.clone(true);
				MCHS.drowNews(item, this);
				MCHS.listenerByItem(item, this);
				$(document).find("input[name=sys-before]").before(item.show());
					MCHS.loadNewsImages(item, this);
				});	
		}, function(err) {
			$('.row').html("Обратитесь к системному администратору");
		});
		MCHS.onclikerNews();
	},

	listenerByItem : function(item, obj) {
		var a_delete = item.find("a[name=delete]");
		a_delete.attr('value', obj.id);
		a_delete.click(function() {
			API.deleteNews({'id':a_delete.attr('value')}, 
				function(resp) {
					if(resp.status === true) {
						item.css('display', 'none');
					} else {
						alert('При удалении произошла ошибка. Обратитесь к системному администратору.');
					}
				},	
				function(err) {
					alert('При удалении произошла ошибка. Обратитесь к системному администратору.');
				});
		});
	},

	drowNews : function(item, obj) {
		item.attr('class', 'sys-news-item well well-sm');
		item.find(".sys-heading-news").html("<span class=\"badge\" style='float: right'>" + obj.date + "</span><center><h1><i>" + obj.title + "</i></h1></center>");
		item.find(".sys-context-news").html(obj.body);
		item.find("a[name=edit]").attr('href', '/news/edit/'+obj.id);
	},

	loadNewsImages : function(item, obj) {
		if(obj.img != null) {
			item.find(".pull-left").show();
			$.each(obj.img, function(index_img) {
				if(index_img == 0) {
					var a_orig = item.find('.sys-origin-img').find('a');
					a_orig.attr('href', this);
					MCHS.tosrusImage(a_orig);
					a_orig.show();
					item.find(".sys-origin-img").find('a').find('img').attr('src', this);
				} 

					var img_prev = $(".sys-news-item-hidden").find(".sys-preview-img").find('a').clone(true);
					img_prev.attr("href", this);
					img_prev.find('img').attr('src', this);
					img_prev.show();
					item.find(".sys-preview-img").append(img_prev);
			});
			MCHS.tosrusImage(item.find('.sys-preview-img a'));
		}
	},

	//FIXME: not right soluction
	scrollNews : function() {
		$(window).scroll(function() {
			if($(".row").height() < $(document).height() - $(window).scrollTop() ) {
				MCHS.loadNews();
			}
		});
	}, 

	dontScrollNews : function() {
		$(window).unbind("scroll");
	}, 

	tosrusImage : function(obj) {

		$(obj).tosrus({
		   buttons    : "inline",
		   pagination : {
		      add     : true,
		      type    : "thumbnails"
		  }
	   });
	},

	onclikerNews : function() {
		
		var show = '<ul class="pager"><li><a>Подробнее</a></li></ul>';
		var hidden = '<ul class="pager"><li><a>Свернуть</a></li></ul>';
		
		$(".sys-all").click(function() {
			$(this).parent().prev().css('height', '400px');
			$(this).html(hidden);
			$(this).click(function(){
				$(this).html(show);
				$(this).parent().prev().css('height', '300px');
				MCHS.onclikerNews();
			});
		});
	},

	onclikerDeleteImage : function() {
		$(".sys-img-edit").click(function() {
			var img_id = $(this).parent().attr('id');
			$(this).parent().css('display', 'none');
				API.deleteImage({'id':img_id}, 
						function(resp) {
							if(resp.status === 'true') {
								
							} else {
								
								alert('При удалении произошла ошибка. Обратитесь к системному администратору.');
							}
						},	
						function(err) {
							alert('При удалении произошла ошибка. Обратитесь к системному администратору.');
						});
			
		});
	},

	validatorNews : function () {
		if (($('input[name=title]').val() !== '')) {
			return true;
		} 
		alert('Заполните поля');
		return false;
	},

	//============================== USERS =================//

	//FIXME
	init_users : function () {
		var users_delete = $(document).find("a[name=sys-delete-user]");
		$.each(users_delete, function(index) {
			$(this).click(function() {
				var user_id = $(this).find('input').attr('value');
				$(this).parent().parent().parent().css('display', 'none');
				var answer = API.deleteUser({'id':user_id}, 
					function(resp) {
						if(resp.status == true) {
						} else {
							alert('Во время удаления произошла ошибка, обратитесь к системному администратору.');
						}
					}, 
					function (err) {
						alert('Во время удаления произошла ошибка, обратитесь к системному администратору.');
					});
			});
		});
	},


	//==================== JOURNAL ====================
	init_journal : function() {
		MCHS.listener_select_journal();
	},

    show_error_search: function () {
		$(".sys-error-search" ).show(1000);
	},
 
  	hide_error_search : function() {
  		  $(".sys-error-search").hide(1000, function() {
		    $( this ).attr('display', 'none');
		  });
  	},
 
	listener_select_journal : function() {

		var division = $('select[name=divisions]');
		var divisionBranch = $('select[name=divisionBranchs]');
		var journals = $('select[name=journals]');
		var month = $('select[name=date]');

		$(".sys-submit-search").click(function() {
			MCHS.hide_error_search();
			var div_id = division.find('option:selected').attr('value');
			var div_brc_id = divisionBranch.find('option:selected').attr('value');
			var journal_id = journals.find('option:selected').attr('value');
			var month_value = month.find('option:selected').attr('value');

			if(div_id == null || div_brc_id == null || journal_id == null || month == null) {
				MCHS.show_error_search();
				return false;
			}

			API.loadJournal({'div_id':div_id, 'div_brc_id' : div_brc_id, 'journal_id': journal_id, 'month' : month_value}, 
				function(resp) {
	

				}, 
				function(err) {

				});
		});

		$(division).change(function() {
  			var value = $(this).find('option:selected').attr('value');
  			API.loadDivisionsBranchsByDivision({'id':value}, 
  				function(resp) {
					divisionBranch.find('option').detach();
					$.each(resp.division_branchs, function(index) {
						divisionBranch.append('<option value=' + this.id + '>' + this.name + '</option>');
					});
					journals.find('option').detach();
					$.each(resp.journals, function(index) {
						journals.append('<option value=' + this.id + '>' + this.date_begin + '</option>');
					});
  				}, 
  				function(err) {
  					//process error
  				});
		});
  		
  		$(divisionBranch).change(function() {
  			var division_id = division.find('option:selected').attr('value');
  			var division_branch_id = $(this).find('option:selected').attr('value');
  			API.processChangeDivisionBranch({'division_id':division_id, 'division_branch_id' : division_branch_id}, 
  				function(resp) {
  					journals.find('option').detach();
					$.each(resp.journals, function(index) {
						journals.append('<option value=' + this.id + '>' + this.date_begin + '</option>');
					});
  				}, 
  				function(err) {

  				});
  		});
 	}, 


 	//======================== WORKBRANCH ======================

 	listener_menu : function() {
 		$('#sys-workbranch-dropdown').click(function() {
 			$(this).next().find('ul').show();
 			$(this).click(function() {
 				$('#sys-workbranch-dropdown').next().find('ul').hide();
 				MCHS.listener_menu();
 			})
 		});
 	},
	
	group_perms : function () {
		console.log('tes');
	}

};




$(document).ready(function() {
	MCHS.init();
});