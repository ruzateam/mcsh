<?php 

class Helper_journal extends Helper {

	public function getListJournal($division_id, $division_branch_id) {
        $Journals = array();         
        $i=0;
        if($division_id <= 0 || $division_branch_id <=0) {
        	return $Journals;
        }
        foreach(ORM::for_table('Journals')->where('division_id', $division_id ) -> where('division_branch_id', $division_branch_id) ->find_result_set() as $Journal) {
            $Journals[$i]['id'] = $Journal->id;    
            $Journals[$i]['date_begin'] = $Journal->date_begin;
            $Journals[$i]['devision_id'] = $Journal->division_id;
            $Journals[$i]['division_branch_id'] = $Journal->division_branch_id;
            $Journals[$i]['master_division_id'] = $Journal->master_division_id;
            $i++;
        }
        return $Journals;
	}
}  