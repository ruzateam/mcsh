<?php

ini_set('display_errors', 1);
//ini_set('error_reporting', E_ALL);

if(version_compare(phpversion(), '5.4.0', '<')) {
    echo 'Требуется PHP не ниже 5.4.0';
    exit;
}
session_start();
define("START", microtime(TRUE));
define("BASE_URL", 'http://'.$_SERVER['HTTP_HOST'].'/');
define("Q_PATH",dirname(__FILE__));
define("ENVIROMENT",'developer');

//LOGS
    function Logs($args) {
        $log = ORM::for_table('Logs')->create();
        $log->log = var_export($args, true);
        $log->save();
    }



include_once "application/engine/bootstrap.php";
